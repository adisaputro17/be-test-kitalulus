package configs

import (
	"net/http"

	"be-test-kitalulus/helpers"
	"be-test-kitalulus/models"
	"be-test-kitalulus/repositories"
	"be-test-kitalulus/services"

	"github.com/gin-gonic/gin"
)

func SetupRoutes(questionRepository *repositories.QuestionRepository) *gin.Engine {
	route := gin.Default()

	// create route /create endpoint
	route.POST("/create", func(context *gin.Context) {
		// initialization question model
		var question models.Question

		if err := context.ShouldBindJSON(&question); err == nil {
			if question.Query == "" || question.CreatedBy == "" {
				context.JSON(http.StatusBadRequest, gin.H{"error": "CreatedBy or Question Field is Empty"})
			} else {
				code := http.StatusOK

				response := services.CreateQuestion(&question, *questionRepository)

				if !response.Success {
					code = http.StatusBadRequest
				}
				context.JSON(code, response)
			}
		} else {
			context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
	})

	route.GET("/", func(context *gin.Context) {
		code := http.StatusOK

		response := services.FindAllQuestions(*questionRepository)

		if !response.Success {
			code = http.StatusBadRequest
		}

		context.JSON(code, response)
	})

	route.GET("/show/:id", func(context *gin.Context) {
		id := context.Param("id")

		code := http.StatusOK

		response := services.FindOneQuestionById(id, *questionRepository)

		if !response.Success {
			code = http.StatusBadRequest
		}

		context.JSON(code, response)
	})

	route.PUT("/update/:id", func(context *gin.Context) {
		id := context.Param("id")

		var question models.Question

		if err := context.ShouldBindJSON(&question); err == nil {
			if question.UpdatedBy == "" {
				context.JSON(http.StatusBadRequest, gin.H{"error": "UpdatedBy Field is Empty"})
			} else {
				code := http.StatusOK

				response := services.UpdateQuestionById(id, &question, *questionRepository)

				if !response.Success {
					code = http.StatusBadRequest
				}
				context.JSON(code, response)
			}
		} else {
			context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
	})

	route.DELETE("/delete/:id", func(context *gin.Context) {
		id := context.Param("id")

		check := services.FindOneQuestionById(id, *questionRepository)

		if check.Success {

			code := http.StatusOK

			response := services.DeleteOneQuestionById(id, *questionRepository)

			if !response.Success {
				code = http.StatusBadRequest
			}

			context.JSON(code, response)

		} else {
			context.JSON(http.StatusBadRequest, gin.H{"error": "record not found"})
		}
	})

	route.GET("/pagination", func(context *gin.Context) {
		code := http.StatusOK

		pagination := helpers.GeneratePaginationRequest(context)

		response := services.Pagination(*questionRepository, context, pagination)

		if !response.Success {
			code = http.StatusBadRequest
		}

		context.JSON(code, response)
	})

	return route
}
