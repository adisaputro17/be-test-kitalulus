package helpers

import (
	"be-test-kitalulus/dtos"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GeneratePaginationRequest(context *gin.Context) *dtos.Pagination {
	// default limit, page & sort parameter
	limit := 2
	page := 0
	sort := "created_at desc"

	query := context.Request.URL.Query()

	for key, value := range query {
		queryValue := value[len(value)-1]

		switch key {
		case "limit":
			limit, _ = strconv.Atoi(queryValue)
			break
		case "page":
			page, _ = strconv.Atoi(queryValue)
			break
		case "sort":
			sort = queryValue
			break
		}
	}

	return &dtos.Pagination{Limit: limit, Page: page, Sort: sort}
}
