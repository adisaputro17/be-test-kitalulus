package repositories

import (
	"be-test-kitalulus/dtos"
	"be-test-kitalulus/models"
	"math"

	"github.com/jinzhu/gorm"
)

type QuestionRepository struct {
	db *gorm.DB
}

func NewQuestionRepository(db *gorm.DB) *QuestionRepository {
	return &QuestionRepository{db: db}
}

func (r *QuestionRepository) Save(question *models.Question) RepositoryResult {
	err := r.db.Save(question).Error

	if err != nil {
		return RepositoryResult{Error: err}
	}

	return RepositoryResult{Result: question}
}

func (r *QuestionRepository) FindAll() RepositoryResult {
	var questions models.Questions

	err := r.db.Find(&questions).Error

	if err != nil {
		return RepositoryResult{Error: err}
	}

	return RepositoryResult{Result: &questions}
}

func (r *QuestionRepository) FindOneById(id string) RepositoryResult {
	var question models.Question

	err := r.db.Where(&models.Question{ID: id}).Take(&question).Error

	if err != nil {
		return RepositoryResult{Error: err}
	}

	return RepositoryResult{Result: &question}
}

func (r *QuestionRepository) DeleteOneById(id string) RepositoryResult {
	err := r.db.Delete(&models.Question{ID: id}).Error

	if err != nil {
		return RepositoryResult{Error: err}
	}

	return RepositoryResult{Result: nil}
}

func (r *QuestionRepository) Pagination(pagination *dtos.Pagination) (RepositoryResult, int) {
	var questions models.Questions

	totalRows, totalPages := 0, 0

	offset := pagination.Page * pagination.Limit

	// get data with limit, offset & order
	find := r.db.Limit(pagination.Limit).Offset(offset).Order(pagination.Sort)

	find = find.Find(&questions)

	// has error find data
	errFind := find.Error

	if errFind != nil {
		return RepositoryResult{Error: errFind}, totalPages
	}

	pagination.Rows = questions

	// count all data
	errCount := r.db.Model(&models.Question{}).Count(&totalRows).Error

	if errCount != nil {
		return RepositoryResult{Error: errCount}, totalPages
	}

	pagination.TotalRows = totalRows

	// calculate total pages
	totalPages = int(math.Ceil(float64(totalRows)/float64(pagination.Limit))) - 1

	return RepositoryResult{Result: pagination}, totalPages
}
