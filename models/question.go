package models

import "time"

type Question struct {
	ID        string `gorm:"primary_key"`
	Query     string `gorm:"type:text;NOT NULL" json:"Question" binding:"required"`
	CreatedAt time.Time
	CreatedBy string `gorm:"type:varchar(255);NOT NULL"`
	UpdatedAt time.Time
	UpdatedBy string
	IsActive  bool
}

type Questions []Question
