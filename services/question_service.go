package services

import (
	"fmt"
	"log"

	"be-test-kitalulus/dtos"
	"be-test-kitalulus/models"
	"be-test-kitalulus/repositories"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func CreateQuestion(question *models.Question, repository repositories.QuestionRepository) dtos.Response {
	uuidResult, err := uuid.NewRandom()

	if err != nil {
		log.Fatalln(err)
	}

	question.ID = uuidResult.String()
	question.IsActive = true

	operationResult := repository.Save(question)

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}

	var data = operationResult.Result.(*models.Question)

	return dtos.Response{Success: true, Data: data}
}

func FindAllQuestions(repository repositories.QuestionRepository) dtos.Response {
	operationResult := repository.FindAll()

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}

	var datas = operationResult.Result.(*models.Questions)

	return dtos.Response{Success: true, Data: datas}
}

func FindOneQuestionById(id string, repository repositories.QuestionRepository) dtos.Response {
	operationResult := repository.FindOneById(id)

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}

	var data = operationResult.Result.(*models.Question)

	return dtos.Response{Success: true, Data: data}
}

func UpdateQuestionById(id string, question *models.Question, repository repositories.QuestionRepository) dtos.Response {
	existingQuestionResponse := FindOneQuestionById(id, repository)

	if !existingQuestionResponse.Success {
		return existingQuestionResponse
	}

	existingQuestion := existingQuestionResponse.Data.(*models.Question)

	existingQuestion.UpdatedBy = question.UpdatedBy
	existingQuestion.Query = question.Query
	existingQuestion.IsActive = question.IsActive

	operationResult := repository.Save(existingQuestion)

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}

	return dtos.Response{Success: true, Data: operationResult.Result}
}

func DeleteOneQuestionById(id string, repository repositories.QuestionRepository) dtos.Response {
	operationResult := repository.DeleteOneById(id)

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}

	return dtos.Response{Success: true}
}

func Pagination(repository repositories.QuestionRepository, context *gin.Context, pagination *dtos.Pagination) dtos.Response {
	operationResult, totalPages := repository.Pagination(pagination)

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}

	var data = operationResult.Result.(*dtos.Pagination)

	// get current url path
	urlPath := context.Request.URL.Path

	// set first & last page pagination response
	data.FirstPage = fmt.Sprintf("%s?limit=%d&page=%d", urlPath, pagination.Limit, 0)
	data.LastPage = fmt.Sprintf("%s?limit=%d&page=%d", urlPath, pagination.Limit, totalPages)

	if data.Page == 0 {
		data.FirstPage = ""
	}

	if data.Page > 0 {
		// set previous page pagination response
		data.PreviousPage = fmt.Sprintf("%s?limit=%d&page=%d", urlPath, pagination.Limit, data.Page-1)
	}

	if data.Page < totalPages {
		// set next page pagination response
		data.NextPage = fmt.Sprintf("%s?limit=%d&page=%d", urlPath, pagination.Limit, data.Page+1)
	}

	if data.Page > totalPages {
		// reset previous page
		data.PreviousPage = ""
	}

	return dtos.Response{Success: true, Data: data}
}
