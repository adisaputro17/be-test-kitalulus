package main

import (
	"log"

	"be-test-kitalulus/configs"
	"be-test-kitalulus/database"
	"be-test-kitalulus/models"
	"be-test-kitalulus/repositories"
)

func main() {
	// database configs
	dbUser, dbPassword, dbName := "root", "", "be_test_db"

	db, err := database.ConnectToDB(dbUser, dbPassword, dbName)

	// unable to connect to database
	if err != nil {
		log.Fatalln(err)
	}

	// ping to database
	err = db.DB().Ping()

	// error ping to database
	if err != nil {
		log.Fatalln(err)
	}

	// migration
	db.AutoMigrate(&models.Question{})

	defer db.Close()

	questionRepository := repositories.NewQuestionRepository(db)

	route := configs.SetupRoutes(questionRepository)

	route.Run(":8000")
}
