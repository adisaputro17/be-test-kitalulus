# be-test kitalulus

The technical test for backend engineers Kitalulus

## Install

1. Clone this repository

        git clone https://gitlab.com/adisaputro17/be-test-kitalulus.git

3. CD to `be-test-kitalulus` folder

        cd be-test-kitalulus

4. Install dependencies

        go get -u github.com/jinzhu/gorm

        go get -u github.com/go-sql-driver/mysql

        go get -u github.com/gin-gonic/gin

        go get github.com/google/uuid

5. Open `main.go` and modify this variable values

        dbUser, dbPassword, dbName := "root", "", "be_test_db"

6. Login to `MySQL` and create the database

        create database be_test_db;

7. Run `main.go`

        go run main.go

## API Documentation

1. Create new question

    ### Request
        POST    /create
    
    ### Request Body Sample
        {
            "CreatedBy":"Adhi Dwi Saputro",
            "Question":"How are you ?"
        }

2. Get list of question with pagination
    ### Request
        GET     /pagination?limit={integer}

3. Get detail question with uuid
    ### Request
        GET     /show/{uuid}

4. Update existing question
    ### Request
        PUT     /update/{uuid}

    ### Request Body Sample
        {
            "UpdatedBy":"Adhi Dwi Saputro",
            "Question":"do you have a problem ?"
        }
5. Delete question
    ### Request
        DELETE      /delete/{uuid}